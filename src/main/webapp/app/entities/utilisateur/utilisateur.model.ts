import { ICommande } from 'app/entities/commande/commande.model';
import { ICommerce } from 'app/entities/commerce/commerce.model';

export interface IUtilisateur {
  id?: string;
  firstname?: string;
  lastname?: string;
  phone?: string | null;
  mail?: string | null;
  address?: string;
  commandes?: ICommande[] | null;
  commerce?: ICommerce[] | null;
}

export class Utilisateur implements IUtilisateur {
  constructor(
    public id?: string,
    public firstname?: string,
    public lastname?: string,
    public phone?: string | null,
    public mail?: string | null,
    public address?: string,
    public commandes?: ICommande[] | null,
    public commerce?: ICommerce[] | null
  ) {}
}

export function getUtilisateurIdentifier(utilisateur: IUtilisateur): string | undefined {
  return utilisateur.id;
}
